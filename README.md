# Sending SMS with Twilio and Mail with Gmail OAuth2

### Overview
this is an example to send SMS with Twilio and mail with Gmail OAuth2 using Node.js.

### API Definition
```text
- POST /send-mail
- POST /send-sms
```

### Installation
clone the repo
```shell
git clone https://alexkurbachou@bitbucket.org/o-element/sendmessages.git
cd sendmessages
```
create a file with the name `.env` in the root project with the following environment variables:
```text
TWILIO_ACCOUNT_SID = <YOUR TWILIO ACCOUNT SID>
TWILIO_AUTH_TOKEN = <YOUR TWILIO AUTH TOKEN>
PHONE_NUMBER = <YOUR PHONE NUMBER>

GMAIL_ADDRESS = <YOUR GMAIL ADDRESS>
GMAIL_CLIENT_ID = <YOUR GMAIL CLIENT ID>
GMAIL_CLIENT_SECRET = <YOUR GMAIL CLIENT SECRET>
GMAIL_REFRESH_TOKEN = <YOUR GMAIL REFRESH TOKEN>
GMAIL_REDIRECT_URL = <YOUR GMAIL REDIRECT URL>
```
then execute:
```shell
npm i
npm start # or npm run dev
```



### Testing through Postman
```text
- POST http://localhost:3000/send-mail

Body JSON example:
[
    {
        "subject": "title",
        "text": "content",
        "emailTo": [
            "test1@example.com"
        ]
    },
    {
        "subject": "title",
        "text": "content",
        "emailTo": [
            "test2@example.com"
        ]
    }
]

Response:
{
    "status": "Success",
    "message": "2 email(s) successfully sent."
}
```

```text
- POST http://localhost:3000//send-sms

Body JSON example:
{
    "message": "Test Message",
    "phone": "+12027953213"
}

Response:
{
    "sid": "SM720d8af458db47bbaa7494158f9db3d4"
}
```
In case of exceeding the limited request, the following message is returned.
```text
You have exceeded the 1 requests in 24 hrs limit!
```
### Copyright and License ###
Copyright ©2020. OpenZNet,Inc. Mountain View, CA 94040 All Rights Reserved. Permission to use, copy, modify, and distribute this software is held by OpenZNet Inc. Contact us: info@openznet.com for commercial licensing opportunities.

IN NO EVENT SHALL OpenZNET BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF OpenZNet HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

OpenZNet SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS". OpenZNet HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.