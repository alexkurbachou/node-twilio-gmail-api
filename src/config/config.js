const twilio = {
  accountSid: process.env.TWILIO_ACCOUNT_SID,
  authToken: process.env.TWILIO_AUTH_TOKEN,
  phoneNumber: process.env.PHONE_NUMBER
};

const mail = {
  senderEmail: process.env.GMAIL_ADDRESS,
  clientId: process.env.GMAIL_CLIENT_ID,
  clientSecret: process.env.GMAIL_CLIENT_SECRET,
  refreshToken: process.env.GMAIL_REFRESH_TOKEN,
  redirectURL: process.env.GMAIL_REDIRECT_URL
};

module.exports = { twilio, mail };