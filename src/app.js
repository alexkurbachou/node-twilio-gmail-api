require("dotenv").config();

const http = require('http');
const express = require("express");
const path = require("path");

const app = express();
const port = process.env.PORT || 3000;

// Settings
app.set("port", port);

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routes
app.use(require("./routes/mail"));
app.use(require("./routes/sms"));

// static files
app.use(express.static(path.join(__dirname, "public")));

const server = http.createServer(app);

server.listen(port);
console.log(`Server running on port ${port}`);
