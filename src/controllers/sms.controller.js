const { sendMessage } = require("../services/twilio/send-sms");

const postMessage = async (req, res) => {
  const { message, phone } = req.body;

  if (!message || !phone) 
    return res.json('missing message or phone');

  // Send an SMS with the message
  const response = await sendMessage(message, phone);

  // log the SMS status
  console.log(`sms id: ${response.sid}`);

  return res.json(response);

};

module.exports = {
  postMessage
};
