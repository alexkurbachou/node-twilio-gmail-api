const axios = require('axios')
const validator = require("email-validator");
const { sendMail } = require("../services/mail/send-mail");

const postMail = async (req, res) => {
  let emails = req.body

  // validate request body
  if (!emails || emails.length < 1) return res.status(400).send("expecting array of emails")
  emails.forEach(email => {
      if (!email.emailTo || email.emailTo.length < 1) return res.status(400).send("at least one emailTo must be supplied in for each email")
      
      email.emailTo.forEach(email => {
          if (!validator.validate(email)) return res.status(400).send("invalid email");
      });

      if (!email.subject) return res.status(400).send("subject must be supplied in for each email")
      if (!email.text) return res.status(400).send("text must be filled in for each email")
  })

  // Send a mail
  const response = await sendMail(emails);

  // Log the mail sendintg status
  console.log(response);

  // Save the mail to the database through API  
  axios
  .post('http://localhost:3000/save-mail', emails)
  .then((res) => {
    console.log(`DB status: ${res.status}`);
  })
  .catch((error) => {
    console.log(error)
  })

  return res.send(response);
};

const saveMail = (req, res) => {
  return res.send(req.body);
};

module.exports = {
  postMail,
  saveMail
};
