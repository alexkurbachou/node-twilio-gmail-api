const rateLimit = require("express-rate-limit");

const HOURS = 24;
const MAX_REQUEST_COUNT = 1;

const smsLimiter = rateLimit({
    windowMs: HOURS * 60 * 60 * 1000, // HOURS hrs in millseconds
    max: MAX_REQUEST_COUNT,
    message: `You have exceeded the ${MAX_REQUEST_COUNT} requests in ${HOURS} hrs limit!`
});

module.exports = smsLimiter;