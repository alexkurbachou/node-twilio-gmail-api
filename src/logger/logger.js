const winston  = require('winston');

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({ filename: './src/logger/info.log' }),
        new winston.transports.File({ filename: './src/logger/error.log', level: 'error' }),
    ],
});

module.exports = logger;