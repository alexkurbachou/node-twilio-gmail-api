const { Router } = require("express");
const emailLimiter = require("../middlewares/emailLimiter")

const router = Router();

const {
  postMail,
  saveMail
} = require("../controllers/mail.controller");

// Send a mail
router.post("/send-mail", emailLimiter, postMail);

router.post("/save-mail", saveMail);

module.exports = router;
