const { Router } = require("express");
const smsLimiter = require("../middlewares/smsLimiter")

const router = Router();

const {
  postMessage
} = require("../controllers/sms.controller");

// Send an SMS
router.post("/send-sms", smsLimiter, postMessage);

module.exports = router;
