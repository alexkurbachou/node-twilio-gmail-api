const config = require("../../config/config");
const nodemailer = require("nodemailer");
const { google } = require("googleapis");

const OAuth2 = google.auth.OAuth2;

const oauth2Client = new OAuth2(
    config.mail.clientId,
    config.mail.clientSecret,
    config.mail.redirectURL
);

oauth2Client.setCredentials({
    refresh_token: config.mail.refreshToken
});

const accessToken = oauth2Client.getAccessToken()

const smtpTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
        type: "OAuth2",
        user: config.mail.senderEmail, 
        clientId: config.mail.clientId,
        clientSecret: config.mail.clientSecret,
        refreshToken: config.mail.refreshToken,
        accessToken: accessToken
    },
    tls: {
        rejectUnauthorized: false
    }
});

/**
 * Send a Mail
 * @param {array} emails - The email list
 */

async function sendMail(emails) {

    let successCnt = 0
    let failCnt = 0
    for (email of emails) {
        for (emailTo of email.emailTo) {

            await smtpTransport.sendMail({
                from: 'info@openznet.com',
                to: emailTo,
                subject: email.subject, 
                text: email.text,
                html: email.html
            }).then(info => {
                console.log("Message sent: %s", info.messageId);
                successCnt++
            }).catch(err => {
                console.log('error emailing', err)
                failCnt++
            });
        }   
    }
        
    let response = {};
    
    if (failCnt == 0)
        response.status = "Success";
    else if (successCnt == 0)
        response.status = "Bad Format";
    else
        response.status = "Partial";

    let failText = failCnt == 0 ? `` : ` ${failCnt} email(s) failed.`
    response.message = `${successCnt} email(s) successfully sent.${failText}`;

    return response;

}

module.exports = { sendMail };