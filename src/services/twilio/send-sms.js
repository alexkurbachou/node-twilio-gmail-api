const config = require("../../config/config");
const client = require("twilio")(config.accountSid, config.authToken);

/**
 * Send a SMS Message
 * @param {string} body - The sms message body
 * @param {string} phone - The sms phone number
 */

async function sendMessage(body, phone) {

  try {

    const message = await client.messages
    .create({
      body,
      from: config.twilio.phoneNumber,
      to: phone
    });

    // console.log(message);
    return { sid: message.sid };

  } catch(err) {

    console.log('sms sending error: ', err);
    return err;
  }

}

module.exports = { sendMessage };
